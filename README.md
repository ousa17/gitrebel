This is a simple app to consume web service from github API V3.

Libraries used on the sample project
------------------------------------
* [AppCompat, CardView, RecyclerView an DesignLibrary](http://developer.android.com/intl/es/tools/support-library/index.html)
* [Data binding](https://erikcaffrey.github.io/ANDROID-databinding-android/)
* [Retrofit 2](http://square.github.io/retrofit/)
* [RxJava & RxAndroid](https://github.com/ReactiveX/RxAndroid)

Developed By
------------

* Ben Wafi Oussama - <oussamabenwafi@gmail.com>

License
-------
    Copyright 2018 Oussama Ben Wafi

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.

