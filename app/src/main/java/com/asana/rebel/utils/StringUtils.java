package com.asana.rebel.utils;

import android.text.TextUtils;

import java.net.URI;
import java.net.URLDecoder;

public class StringUtils {

    public static final String UTF8 = "UTF-8";

    public static String decodeURL(String str) {
        String decoded = str;
        if (isNotEmpty(str)) {
            try {
                decoded = URLDecoder.decode(str, UTF8);
            } catch (Exception e) {
                decoded = str;
            }
        }
        return decoded;
    }

    public static String decodeUTF8(String str) {
        String decoded = str;
        if (isNotEmpty(str)) {
            byte[] src = str.getBytes();
            try {
                decoded = decodeURL(new String(src, UTF8));
            } catch (Exception e) {
                decoded = str;
            }
        }
        return decoded;
    }

    public static String upperFirstChar(String str) {
        return isNotEmpty(str) ? str.substring(0, 1).toUpperCase() + str.substring(1) : str;
    }

    public static String upperCase(String str) {
        return isNotEmpty(str) ? str.toUpperCase() : str;
    }

    public static String lowerCase(String str) {
        return isNotEmpty(str) ? str.toLowerCase() : str;
    }

    public static boolean isEmpty(CharSequence str) {
        return TextUtils.isEmpty(str);
    }

    public static boolean equals(String a, String b) {
        return TextUtils.equals(a, b);
    }

    public static boolean equals(CharSequence a, CharSequence b) {
        return TextUtils.equals(a, b);
    }

    public static boolean isNotEmpty(CharSequence str) {
        return str!= null && !isEmpty(str) ;
    }

    public static String trim(String str) {
        return (isNotEmpty(str)) ? str.trim().replaceAll("\\s+", "") : str;
    }

    public static String trimAndReplace(String str) {
        return (isNotEmpty(str)) ? trim(str).replace("/", "") : str;
    }

    public static String getDomainName(String url) {
        String domain = "";
        URI uri;
        if (StringUtils.isNotEmpty(url)) {
            try {
                uri = new URI(url);
                domain = uri.getHost().startsWith("www.") ? uri.getHost().substring(4) : uri.getHost();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return domain;
    }

    public static String getStringNotNull(String str) {
        if (str != null)
            return str;
        else
            return "";
    }

    public static String capitalizeFirstletter(String str){
        if(str!=null){
            return  str.substring(0, 1).toUpperCase() + str.substring(1);
        }else  return "";
    }

    public static String allSpace(String s) {
        StringBuilder sb = new StringBuilder(s.length());
        for (int i = 0; i < s.length(); i++) {
            sb.append('*');
        }
        return sb.toString();
    }

}
