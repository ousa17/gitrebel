package com.asana.rebel.utils;

import android.annotation.SuppressLint;
import android.util.Log;

import com.asana.rebel.BuildConfig;


@SuppressLint("LogConditional")
public class Logger {

    private static final String APP_TAG = "BeeTheMove:";

    public static <T> void i(String tag, T msg) {
        if (BuildConfig.IS_LOGGABLE) Log.i(APP_TAG + tag, msg + "");
    }

    public static <T> void d(String tag, T msg) {
        if (BuildConfig.IS_LOGGABLE) Log.d(APP_TAG + tag, msg + "");
    }

    public static <T> void v(String tag, T msg) {
        if (BuildConfig.IS_LOGGABLE) Log.v(APP_TAG + tag, msg + "");
    }

    public static <T> void e(String tag, T msg) {
        if (BuildConfig.IS_LOGGABLE) Log.e(APP_TAG + tag, msg + "");
    }

    public static <T> void w(String tag, T msg) {
        if (BuildConfig.IS_LOGGABLE) Log.w(APP_TAG + tag, msg + "");
    }

    public static <T> void wtf(String tag, T msg) {
        if (BuildConfig.IS_LOGGABLE) Log.wtf(APP_TAG + tag, msg + "");
    }

    public static <T> void i(T msg) {
        if (BuildConfig.IS_LOGGABLE) Log.i(APP_TAG, msg + "");
    }

    public static <T> void d(T msg) {
        if (BuildConfig.IS_LOGGABLE) Log.d(APP_TAG, msg + "");
    }

    public static <T> void v(T msg) {
        if (BuildConfig.IS_LOGGABLE) Log.v(APP_TAG, msg + "");
    }

    public static <T> void e(T msg) {
        if (BuildConfig.IS_LOGGABLE) Log.e(APP_TAG, msg + "");
    }

    public static <T> void w(T msg) {
        if (BuildConfig.IS_LOGGABLE) Log.w(APP_TAG, msg + "");
    }

    public static <T> void wtf(T msg) {
        if (BuildConfig.IS_LOGGABLE) Log.wtf(APP_TAG, msg + "");
    }

    public static void error(Object tag, Throwable throwable) {
        if (BuildConfig.IS_LOGGABLE)
            Log.e(APP_TAG + tag.getClass().getSimpleName(), ExceptionUtils.format(throwable));
    }

    public static void error(String tag, Throwable throwable) {
        if (BuildConfig.IS_LOGGABLE) Log.e(APP_TAG + tag, ExceptionUtils.format(throwable));
    }
}
