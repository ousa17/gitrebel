package com.asana.rebel.modules.listRepos;

/**
 * Created by oussama on 27/01/2018.
 */


import android.content.Context;
import android.databinding.ObservableField;
import android.databinding.ObservableInt;
import android.support.annotation.NonNull;
import android.view.View;
import com.asana.rebel.App;
import com.asana.rebel.R;
import com.asana.rebel.data.models.GitRepo;
import com.asana.rebel.data.source.repository.GitRepoRepository;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;

import javax.inject.Inject;


public class GitRepoViewModel extends Observable {

  @Inject
  GitRepoRepository gitRepoRepository;

  public ObservableInt gitProgress;
  public ObservableInt gitRecycler;
  public ObservableInt gitLabel;
  public ObservableField<String> messageLabel;

  private List<GitRepo> gitRepos;
  private Context context;
  private CompositeDisposable compositeDisposable = new CompositeDisposable();

  public GitRepoViewModel(@NonNull Context context) {
    App.getDataComponent().inject(this);
    this.context = context;
    this.gitRepos = new ArrayList<>();
    gitProgress = new ObservableInt(View.GONE);
    gitRecycler = new ObservableInt(View.GONE);
    gitLabel = new ObservableInt(View.VISIBLE);
    messageLabel = new ObservableField<>(context.getString(R.string.default_loading_git_repo));
  }

  public void onClickFabLoad(View view) {
    initializeViews();
    fetchRepoList();
  }

  //It is "public" to show an example of test
  public void initializeViews() {
    gitLabel.set(View.GONE);
    gitRecycler.set(View.GONE);
    gitProgress.set(View.VISIBLE);
  }

  public void fetchRepoList() {
    compositeDisposable.add(gitRepoRepository.getListRepo()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeWith(new DisposableObserver<List<GitRepo>>() {
              @Override
              public void onNext(List<GitRepo> gitRepos) {
                changeGitRepoDataSet(gitRepos);
                gitProgress.set(View.GONE);
                gitLabel.set(View.GONE);
                gitRecycler.set(View.VISIBLE);
              }

              @Override
              public void onError(Throwable e) {
                messageLabel.set(context.getString(R.string.error_loading_git_repo));
                gitProgress.set(View.GONE);
                gitLabel.set(View.VISIBLE);
                gitRecycler.set(View.GONE);
              }

              @Override
              public void onComplete() {

              }
            })
    );
  }

  private void changeGitRepoDataSet(List<GitRepo> gitRepos) {
    this.gitRepos.addAll(gitRepos);
    setChanged();
    notifyObservers();
  }

  public List<GitRepo> getGitRepos() {
    return gitRepos;
  }

  private void unSubscribeFromObservable() {
    if (compositeDisposable != null && !compositeDisposable.isDisposed()) {
      compositeDisposable.dispose();
    }
  }

  public void reset() {
    unSubscribeFromObservable();
    compositeDisposable = null;
    context = null;
  }
}
