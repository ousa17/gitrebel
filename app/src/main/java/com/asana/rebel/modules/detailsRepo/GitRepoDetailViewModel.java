package com.asana.rebel.modules.detailsRepo;

/**
 * Created by oussama on 27/01/2018.
 */

import android.databinding.BindingAdapter;
import android.view.View;
import android.widget.ImageView;
import com.asana.rebel.data.models.GitRepo;
import com.bumptech.glide.Glide;

public class GitRepoDetailViewModel {

  private GitRepo gitRepo;

  public GitRepoDetailViewModel(GitRepo gitRepo) {
    this.gitRepo = gitRepo;
  }

  public String getFullUserName() {
    return gitRepo.getFullName();
  }

  public String getUserName() {
    return gitRepo.getName();
  }

  public String getEmail() {
    return gitRepo.getOwner().getAvatarUrl();
  }

  public int getEmailVisibility() {
    return View.VISIBLE ;
  }

  public String getCell() {
    return gitRepo.getFullName();
  }

  public String getPictureProfile() {
    return gitRepo.getOwner().getAvatarUrl();
  }

  public String getAddress() {
    return gitRepo.getDescription();
  }

  public String getGender() {
    return gitRepo.getFullName();
  }

  @BindingAdapter({"imageUrl"})
  public static void loadImage(ImageView view, String imageUrl) {
    Glide.with(view.getContext()).load(imageUrl).into(view);
  }
}
