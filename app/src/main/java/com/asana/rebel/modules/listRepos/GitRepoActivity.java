package com.asana.rebel.modules.listRepos;

/**
 * Created by oussama on 27/01/2018.
 */


import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import com.asana.rebel.R;
import com.asana.rebel.databinding.GitRepoActivityBinding;

import java.util.Observable;
import java.util.Observer;

public class GitRepoActivity extends AppCompatActivity implements Observer {

  private GitRepoActivityBinding gitRepoActivityBinding;
  private GitRepoViewModel gitRepoViewModel;

  @Override protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    initDataBinding();
    setSupportActionBar(gitRepoActivityBinding.toolbar);
    setupListRepoView(gitRepoActivityBinding.listGitRepo);
    setupObserver(gitRepoViewModel);
  }

  private void initDataBinding() {
    gitRepoActivityBinding = DataBindingUtil.setContentView(this, R.layout.git_repo_activity);
    gitRepoViewModel = new GitRepoViewModel(this);
    gitRepoActivityBinding.setMainViewModel(gitRepoViewModel);
  }

  private void setupListRepoView(RecyclerView recyclerView) {
    GitRepoAdapter adapter = new GitRepoAdapter();
    recyclerView.setAdapter(adapter);
    recyclerView.setLayoutManager(new LinearLayoutManager(this));
  }

  public void setupObserver(Observable observable) {
    observable.addObserver(this);
  }

  @Override protected void onDestroy() {
    super.onDestroy();
    gitRepoViewModel.reset();
  }


  @Override public void update(Observable observable, Object data) {
    if (observable instanceof GitRepoViewModel) {
      GitRepoAdapter gitRepoAdapter = (GitRepoAdapter) gitRepoActivityBinding.listGitRepo.getAdapter();
      GitRepoViewModel gitRepoViewModel = (GitRepoViewModel) observable;
      gitRepoAdapter.setGitRepos(gitRepoViewModel.getGitRepos());
    }
  }
}
