package com.asana.rebel.modules.detailsRepo;

/**
 * Created by oussama on 27/01/2018.
 */

import android.content.Context;
import android.databinding.ObservableField;
import android.databinding.ObservableInt;
import android.support.annotation.NonNull;
import android.view.View;

import com.asana.rebel.App;
import com.asana.rebel.R;
import com.asana.rebel.data.models.GitRepo;
import com.asana.rebel.data.models.Owner;
import com.asana.rebel.data.source.repository.GitRepoRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;


public class SubscribersViewModel extends Observable {

  @Inject
  GitRepoRepository gitRepoRepository;

  public ObservableInt gitProgress;
  public ObservableInt gitRecycler;
  public ObservableInt gitLabel;
  public ObservableField<String> messageLabel;

  private List<Owner> subscribers;
  private Context context;
  private CompositeDisposable compositeDisposable = new CompositeDisposable();

  private final GitRepo gitRepo;


  public SubscribersViewModel(@NonNull Context context, GitRepo gitRepo) {
    App.getDataComponent().inject(this);
    this.context = context;
    this.subscribers = new ArrayList<>();
    this.gitRepo = gitRepo;
    gitProgress = new ObservableInt(View.GONE);
    gitRecycler = new ObservableInt(View.GONE);
    gitLabel = new ObservableInt(View.VISIBLE);
    messageLabel = new ObservableField<>(context.getString(R.string.default_loading_git_repo));
    initializeViews();
    fetchSubscribersList();
  }


  public void initializeViews() {
    gitLabel.set(View.GONE);
    gitRecycler.set(View.GONE);
    gitProgress.set(View.VISIBLE);
  }

  public void fetchSubscribersList() {
    compositeDisposable.add(gitRepoRepository.getListSubscribers(gitRepo.getSubscribersUrl())
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeWith(new DisposableObserver<List<Owner>>() {
              @Override
              public void onNext(List<Owner> subscribers) {
                changeGitRepoDataSet(subscribers);
                gitProgress.set(View.GONE);
                gitLabel.set(View.GONE);
                gitRecycler.set(View.VISIBLE);
              }

              @Override
              public void onError(Throwable e) {
                messageLabel.set(context.getString(R.string.error_loading_git_repo));
                gitProgress.set(View.GONE);
                gitLabel.set(View.VISIBLE);
                gitRecycler.set(View.GONE);
              }

              @Override
              public void onComplete() {

              }
            })
    );
  }

  private void changeGitRepoDataSet(List<Owner> subscribers) {
    this.subscribers.addAll(subscribers);
    setChanged();
    notifyObservers();
  }

  public List<Owner> getListSubscribers() {
    return subscribers;
  }

  private void unSubscribeFromObservable() {
    if (compositeDisposable != null && !compositeDisposable.isDisposed()) {
      compositeDisposable.dispose();
    }
  }

  public void reset() {
    unSubscribeFromObservable();
    compositeDisposable = null;
    context = null;
  }
}
