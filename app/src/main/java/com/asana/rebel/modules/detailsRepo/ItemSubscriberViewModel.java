package com.asana.rebel.modules.detailsRepo;

/**
 * Created by oussama on 27/01/2018.
 */

import android.content.Context;
import android.databinding.BaseObservable;
import android.databinding.BindingAdapter;
import android.view.View;
import android.widget.ImageView;

import com.asana.rebel.data.models.GitRepo;
import com.asana.rebel.data.models.Owner;
import com.bumptech.glide.Glide;


public class ItemSubscriberViewModel extends BaseObservable {

  private Owner subscriber;
  private Context context;

  public ItemSubscriberViewModel(Owner subscriber, Context context) {
    this.subscriber = subscriber;
    this.context = context;
  }

  public String getFullName() {
    return subscriber.getLogin();
  }


  public String getOwnerAvatar() {
    return subscriber.getAvatarUrl();
  }

  @BindingAdapter("imageUrl") public static void setImageUrl(ImageView imageView, String url) {
    Glide.with(imageView.getContext()).load(url).into(imageView);
  }


  public void setSubscriber(Owner owner) {
    this.subscriber = owner;
    notifyChange();
  }
}
