package com.asana.rebel.modules.detailsRepo;

/**
 * Created by oussama on 27/01/2018.
 */

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.asana.rebel.R;
import com.asana.rebel.data.models.GitRepo;
import com.asana.rebel.databinding.GitRepoDetailActivityBinding;

import java.util.Observer;

public class GitRepoDetailActivity extends AppCompatActivity implements Observer{

  private static final String EXTRA_GIT_REPO = "EXTRA_GIT_REPO";

  private GitRepoDetailActivityBinding gitRepoDetailActivityBinding;
  private SubscribersViewModel subscribersViewModel;
  private GitRepo gitRepo;

  @Override protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    gitRepoDetailActivityBinding =
        DataBindingUtil.setContentView(this, R.layout.git_repo_detail_activity);

    getExtrasFromIntent();


    subscribersViewModel = new SubscribersViewModel(this,gitRepo);
    gitRepoDetailActivityBinding.setMainViewModel(subscribersViewModel);
    setSupportActionBar(gitRepoDetailActivityBinding.toolbar);
    displayHomeAsUpEnabled();

    setupListSubscribersView(gitRepoDetailActivityBinding.listGitRepo);
    setupObserver(subscribersViewModel);
  }

  private void setupListSubscribersView(RecyclerView recyclerView) {
    SubscribersAdapter adapter = new SubscribersAdapter();
    recyclerView.setAdapter(adapter);
    recyclerView.setLayoutManager(new LinearLayoutManager(this));
  }

  public void setupObserver(java.util.Observable observable) {
    observable.addObserver(this);
  }

  @Override protected void onDestroy() {
    super.onDestroy();
    subscribersViewModel.reset();
  }

  public static Intent launchDetail(Context context, GitRepo people) {
    Intent intent = new Intent(context, GitRepoDetailActivity.class);
    intent.putExtra(EXTRA_GIT_REPO, people);
    return intent;
  }

  private void displayHomeAsUpEnabled() {
    ActionBar actionBar = getSupportActionBar();
    if (actionBar != null) {
      actionBar.setDisplayHomeAsUpEnabled(true);
    }
  }

  private void getExtrasFromIntent() {
    gitRepo = getIntent().getParcelableExtra(EXTRA_GIT_REPO);
    GitRepoDetailViewModel gitRepoDetailViewModel = new GitRepoDetailViewModel(gitRepo);
    gitRepoDetailActivityBinding.setViewModel(gitRepoDetailViewModel);
  }

  @Override
  public void update(java.util.Observable observable, Object data) {
    if (observable instanceof SubscribersViewModel) {
      SubscribersAdapter subscribersAdapter = (SubscribersAdapter) gitRepoDetailActivityBinding.listGitRepo.getAdapter();
      SubscribersViewModel subscribersViewModel = (SubscribersViewModel) observable;
      subscribersAdapter.setSubscribers(subscribersViewModel.getListSubscribers());
    }
  }
}
