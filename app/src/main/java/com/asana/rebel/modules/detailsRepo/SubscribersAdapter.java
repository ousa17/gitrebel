package com.asana.rebel.modules.detailsRepo;

/**
 * Created by oussama on 27/01/2018.
 */

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.asana.rebel.R;
import com.asana.rebel.data.models.GitRepo;
import com.asana.rebel.data.models.Owner;
import com.asana.rebel.databinding.ItemSubscriberBinding;

import java.util.Collections;
import java.util.List;

public class SubscribersAdapter extends RecyclerView.Adapter<SubscribersAdapter.SubscriberAdapterViewHolder> {

  private List<Owner> subscribers;

  public SubscribersAdapter() {
    this.subscribers = Collections.emptyList();
  }

  @Override public SubscriberAdapterViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    ItemSubscriberBinding itemSubscriberBinding =
        DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_subscriber,
            parent, false);
    return new SubscriberAdapterViewHolder(itemSubscriberBinding);
  }

  @Override public void onBindViewHolder(SubscriberAdapterViewHolder holder, int position) {
    holder.bindSubscriber(subscribers.get(position));
  }

  @Override public int getItemCount() {
    return subscribers.size();
  }

  public void setSubscribers(List<Owner> subscribers) {
    this.subscribers = subscribers;
    notifyDataSetChanged();
  }

  public static class SubscriberAdapterViewHolder extends RecyclerView.ViewHolder {
    ItemSubscriberBinding mItemSubscriberBinding;

    public SubscriberAdapterViewHolder(ItemSubscriberBinding ItemSubscriberBinding) {
      super(ItemSubscriberBinding.itemSubscriber);
      this.mItemSubscriberBinding = ItemSubscriberBinding;
    }

    void bindSubscriber(Owner subscriber) {
      if (mItemSubscriberBinding.getSubscriberViewModel() == null) {
        mItemSubscriberBinding.setSubscriberViewModel(
            new ItemSubscriberViewModel(subscriber, itemView.getContext()));
      } else {
        mItemSubscriberBinding.getSubscriberViewModel().setSubscriber(subscriber);
      }
    }
  }
}
