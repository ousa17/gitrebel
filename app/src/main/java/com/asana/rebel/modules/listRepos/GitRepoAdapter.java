package com.asana.rebel.modules.listRepos;

/**
 * Created by oussama on 27/01/2018.
 */


import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.asana.rebel.R;
import com.asana.rebel.data.models.GitRepo;
import com.asana.rebel.databinding.ItemGitRepoBinding;

import java.util.Collections;
import java.util.List;

public class GitRepoAdapter extends RecyclerView.Adapter<GitRepoAdapter.GitRepoAdapterViewHolder> {

  private List<GitRepo> gitRepos;

  public GitRepoAdapter() {
    this.gitRepos = Collections.emptyList();
  }

  @Override public GitRepoAdapterViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    ItemGitRepoBinding itemGitRepoBinding =
        DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_git_repo,
            parent, false);
    return new GitRepoAdapterViewHolder(itemGitRepoBinding);
  }

  @Override public void onBindViewHolder(GitRepoAdapterViewHolder holder, int position) {
    holder.bindGitRepo(gitRepos.get(position));
  }

  @Override public int getItemCount() {
    return gitRepos.size();
  }

  public void setGitRepos(List<GitRepo> gitRepos) {
    this.gitRepos = gitRepos;
    notifyDataSetChanged();
  }

  public static class GitRepoAdapterViewHolder extends RecyclerView.ViewHolder {
    ItemGitRepoBinding mItemGitRepoBinding;

    public GitRepoAdapterViewHolder(ItemGitRepoBinding itemGitRepoBinding) {
      super(itemGitRepoBinding.itemRepoGit);
      this.mItemGitRepoBinding = itemGitRepoBinding;
    }

    void bindGitRepo(GitRepo gitRepo) {
      if (mItemGitRepoBinding.getGitRepoViewModel() == null) {
        mItemGitRepoBinding.setGitRepoViewModel(
            new ItemGitRepoViewModel(gitRepo, itemView.getContext()));
      } else {
        mItemGitRepoBinding.getGitRepoViewModel().setReposiory(gitRepo);
      }
    }
  }
}
