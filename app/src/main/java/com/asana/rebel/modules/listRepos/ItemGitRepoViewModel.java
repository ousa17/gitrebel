package com.asana.rebel.modules.listRepos;

/**
 * Created by oussama on 27/01/2018.
 */


import android.content.Context;
import android.databinding.BaseObservable;
import android.databinding.BindingAdapter;
import android.view.View;
import android.widget.ImageView;

import com.asana.rebel.data.models.GitRepo;
import com.bumptech.glide.Glide;
import com.asana.rebel.modules.detailsRepo.GitRepoDetailActivity;


public class ItemGitRepoViewModel extends BaseObservable {

  private GitRepo reposiory;
  private Context context;

  public ItemGitRepoViewModel(GitRepo reposiory, Context context) {
    this.reposiory = reposiory;
    this.context = context;
  }

  public String getFullName() {
    return reposiory.getName();
  }

  public String getDescription() {
    return reposiory.getDescription();
  }


  public String getOwnerAvatar() {
    return reposiory.getOwner().getAvatarUrl();
  }

  @BindingAdapter("imageUrl") public static void setImageUrl(ImageView imageView, String url) {
    Glide.with(imageView.getContext()).load(url).into(imageView);
  }

  public void onItemClick(View view) {
    context.startActivity(GitRepoDetailActivity.launchDetail(view.getContext(), reposiory));
  }

  public void setReposiory(GitRepo reposiory) {
    this.reposiory = reposiory;
    notifyChange();
  }
}
