package com.asana.rebel;

import android.app.Application;
import android.support.v7.app.AppCompatDelegate;

import com.asana.rebel.data.DaggerDataComponent;
import com.asana.rebel.data.DataComponent;
import com.asana.rebel.data.source.local.database.StorageModule;
import com.asana.rebel.data.source.remote.service.NetworkModule;
import com.asana.rebel.data.source.remote.service.ServiceEndpoint;

public class App extends Application {


  private static DataComponent dataComponent;

  static {
    AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
  }

  public static DataComponent getDataComponent() {
    return dataComponent;
  }

  @Override
  public void onCreate() {
    initAppComponent();
    super.onCreate();
  }

  private void initAppComponent() {
    dataComponent = DaggerDataComponent.builder()
            .appModule(new AppModule(this))
            .networkModule(new NetworkModule(ServiceEndpoint.BASE_URL))
            .storageModule(new StorageModule())
            .build();
  }
}
