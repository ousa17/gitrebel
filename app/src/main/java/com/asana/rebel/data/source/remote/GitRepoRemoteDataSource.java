package com.asana.rebel.data.source.remote;

import android.support.annotation.NonNull;

import com.asana.rebel.data.models.GitRepo;
import com.asana.rebel.data.models.Owner;
import com.asana.rebel.data.source.remote.service.ServiceEndpoint;
import com.asana.rebel.data.source.repository.GitRepoDataSource;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Observable;

/**
 * Created by user on 27/01/2018.
 */

@Singleton
public class GitRepoRemoteDataSource implements GitRepoDataSource {


    private final ServiceEndpoint serviceEndpoint;

    @Inject
    public GitRepoRemoteDataSource(@NonNull ServiceEndpoint serviceEndpoint) {
        this.serviceEndpoint = serviceEndpoint;
    }

    @Override
    public Observable<List<GitRepo>> getListRepo() {
        return serviceEndpoint.getListRepo();
    }

    @Override
    public Observable<List<Owner>> getListSubscribers(String subscribersUrl) {
        return serviceEndpoint.getListSubscribers(subscribersUrl);

    }

}
