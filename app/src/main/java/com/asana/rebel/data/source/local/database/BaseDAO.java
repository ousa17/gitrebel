package com.asana.rebel.data.source.local.database;

import com.asana.rebel.utils.Logger;
import com.j256.ormlite.dao.Dao;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.concurrent.Callable;

public class BaseDAO<T, ID> {

    protected Dao<T, ID> dao;

    public BaseDAO(DataBaseHelper dataBaseHelper, Class<T> clazz) {
        try {
            dao = dataBaseHelper.getsDao(clazz);
        } catch (SQLException e) {
            Logger.error(this, e);
        }
    }

    public void create(T object) {
        try {
            dao.create(object);
        } catch (SQLException e) {
            Logger.error(this, e);
        }
    }

    public void create(final Collection<T> objects) {
        try {
            dao.callBatchTasks(new Callable<Void>() {
                @Override
                public Void call() throws Exception {
                    for (T object : objects) {
                        dao.create(object);
                    }
                    return null;
                }
            });
        } catch (Exception e) {
            Logger.error(this, e);
        }
    }

    public void createOrUpdate(final Collection<T> objects) {
        try {
            dao.callBatchTasks(new Callable<Void>() {
                @Override
                public Void call() throws Exception {
                    for (T object : objects) {
                        dao.createOrUpdate(object);
                    }
                    return null;
                }
            });
        } catch (Exception e) {
            Logger.error(this, e);
        }
    }

    public void createOrUpdate(T object) {
        try {
            dao.createOrUpdate(object);
        } catch (SQLException e) {
            Logger.error(this, e);
        }
    }

    public void createIfNotExists(T object) {
        try {
            dao.createIfNotExists(object);
        } catch (SQLException e) {
            Logger.error(this, e);
        }
    }

    public void createIfNotExists(final Collection<T> objects) {
        try {
            dao.callBatchTasks(new Callable<Void>() {
                @Override
                public Void call() throws Exception {
                    for (T object : objects) {
                        dao.createIfNotExists(object);
                    }
                    return null;
                }
            });
        } catch (Exception e) {
            Logger.error(this, e);
        }
    }

    public void update(T object) {
        try {
            dao.update(object);
        } catch (SQLException e) {
            Logger.error(this, e);
        }
    }

    public void delete(T object) {
        try {
            dao.delete(object);
        } catch (SQLException e) {
            Logger.error(this, e);
        }
    }

    public void deleteById(ID id) {
        try {
            dao.deleteById(id);
        } catch (SQLException e) {
            Logger.error(this, e);
        }
    }

    public void deleteIds(Collection<ID> ids) {
        try {
            dao.deleteIds(ids);
        } catch (SQLException e) {
            Logger.error(this, e);
        }
    }

    public void deleteAll() {
        try {
            dao.deleteBuilder().delete();
        } catch (SQLException e) {
            Logger.error(this, e);
        }
    }

    public Collection<T> findALl() {
        Collection<T> events;
        try {
            events = dao.queryForAll();
        } catch (SQLException e) {
            events = new ArrayList<>();
            Logger.error(this, e);
        }
        return events;
    }

    public T findById(ID id) {
        T object;
        try {
            object = dao.queryForId(id);
        } catch (SQLException e) {
            object = null;
            Logger.error(this, e);
        }
        return object;
    }

}
