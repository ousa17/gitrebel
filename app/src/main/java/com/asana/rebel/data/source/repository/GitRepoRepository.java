package com.asana.rebel.data.source.repository;

import android.support.annotation.NonNull;

import com.asana.rebel.data.models.GitRepo;
import com.asana.rebel.data.models.Owner;
import com.asana.rebel.data.source.local.GitRepoLocalDataSource;
import com.asana.rebel.data.source.remote.GitRepoRemoteDataSource;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Observable;

/**
 * Created by user on 27/01/2018.
 */

@Singleton
public class GitRepoRepository implements  GitRepoDataSource {

    private final GitRepoRemoteDataSource remoteDataSource;
    private final GitRepoLocalDataSource localDataSource;

    @Inject
    public GitRepoRepository(@NonNull GitRepoLocalDataSource localDataSource, @NonNull GitRepoRemoteDataSource remoteDataSource) {
        this.localDataSource = localDataSource;
        this.remoteDataSource = remoteDataSource;
    }

    @Override
    public Observable<List<GitRepo>> getListRepo() {
        return remoteDataSource.getListRepo();
    }

    public Observable<List<Owner>> getListSubscribers(String subscribersUrl) {
        return remoteDataSource.getListSubscribers(subscribersUrl);
    }
}
