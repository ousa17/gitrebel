package com.asana.rebel.data.source.remote.service;

import android.support.annotation.NonNull;

import com.asana.rebel.App;

import java.io.IOException;

import okhttp3.Headers;
import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;


public class RequestInterceptor implements Interceptor {


    public RequestInterceptor() {
        App.getDataComponent().inject(this);
    }

    public static Request provideRequest(@NonNull Request original, @NonNull Headers headers, @NonNull HttpUrl httpUrl) {
        Request.Builder requestBuilder = original.newBuilder()
               // .headers(headers)
                // .url(httpUrl)
                .method(original.method(), original.body());
        return requestBuilder.build();

    }


    public static HttpUrl provideHttpUrl(@NonNull Request original) {
        HttpUrl.Builder httpUrlBuilder = original.url().newBuilder() ;
        return httpUrlBuilder.build();
    }

    @Override
    public Response intercept(@NonNull Chain chain) throws IOException {
        final Request request = chain.request();
        final HttpUrl httpUrl = provideHttpUrl(request);
        final Request newRequest = provideRequest(request, null, httpUrl);
        return chain.proceed(newRequest);
    }

}
