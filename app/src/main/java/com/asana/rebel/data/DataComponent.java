package com.asana.rebel.data;

/**
 * Created by oussama on 27/01/2018.
 */

import android.support.annotation.NonNull;

import com.asana.rebel.AppModule;
import com.asana.rebel.data.source.local.database.StorageModule;
import com.asana.rebel.data.source.remote.service.NetworkModule;
import com.asana.rebel.data.source.remote.service.RequestInterceptor;
import com.asana.rebel.modules.detailsRepo.SubscribersViewModel;
import com.asana.rebel.modules.listRepos.GitRepoViewModel;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {AppModule.class, StorageModule.class, NetworkModule.class})
public interface DataComponent {

    void inject(@NonNull RequestInterceptor requestInterceptor);

    void inject(@NonNull GitRepoViewModel gitRepoViewModel);

    void inject(@NonNull SubscribersViewModel subscribersViewModel);



}
