package com.asana.rebel.data.source.local.database;

import android.content.Context;
import android.support.annotation.NonNull;

import com.fasterxml.jackson.databind.ObjectMapper;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class StorageModule {

    @Provides
    @Singleton
    public SharedPreference provideSharedPreferences(@NonNull Context context, @NonNull ObjectMapper objectMapper) {
        return new SharedPreference(context, objectMapper);
    }

    @Provides
    @Singleton
    public DataBaseHelper provideDataBaseHelper(@NonNull Context context) {
        return new DataBaseHelper(context);
    }
}
