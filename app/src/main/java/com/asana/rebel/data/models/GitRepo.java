package com.asana.rebel.data.models;

/**
 * Created by oussama on 27/01/2018.
 */

import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.j256.ormlite.field.DatabaseField;


public class GitRepo implements Parcelable {

    public GitRepo() {
    }

    @DatabaseField(id = true)
    @JsonProperty("id")
    private Integer id;
    @JsonProperty("name")
    private String name;
    @JsonProperty("full_name")
    private String fullName;
    @JsonProperty("owner")
    private Owner owner;
    @JsonProperty("forks_count")
    private Integer forksCount;
    @JsonProperty("subscribers_count")
    private Integer subscribersCount;
    @JsonProperty("description")
    private String description;
    @JsonProperty("subscribers_url")
    private String subscribersUrl;


    protected GitRepo(Parcel in) {
        if (in.readByte() == 0) {
            id = null;
        } else {
            id = in.readInt();
        }
        name = in.readString();
        fullName = in.readString();
        owner = in.readParcelable(Owner.class.getClassLoader());
        if (in.readByte() == 0) {
            forksCount = null;
        } else {
            forksCount = in.readInt();
        }
        if (in.readByte() == 0) {
            subscribersCount = null;
        } else {
            subscribersCount = in.readInt();
        }
        description = in.readString();
        subscribersUrl = in.readString();
    }

    public static final Creator<GitRepo> CREATOR = new Creator<GitRepo>() {
        @Override
        public GitRepo createFromParcel(Parcel in) {
            return new GitRepo(in);
        }

        @Override
        public GitRepo[] newArray(int size) {
            return new GitRepo[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        if (id == null) {
            parcel.writeByte((byte) 0);
        } else {
            parcel.writeByte((byte) 1);
            parcel.writeInt(id);
        }
        parcel.writeString(name);
        parcel.writeString(fullName);
        parcel.writeParcelable(owner, i);
        if (forksCount == null) {
            parcel.writeByte((byte) 0);
        } else {
            parcel.writeByte((byte) 1);
            parcel.writeInt(forksCount);
        }
        if (subscribersCount == null) {
            parcel.writeByte((byte) 0);
        } else {
            parcel.writeByte((byte) 1);
            parcel.writeInt(subscribersCount);
        }
        parcel.writeString(description);
        parcel.writeString(subscribersUrl);
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public Owner getOwner() {
        return owner;
    }

    public void setOwner(Owner owner) {
        this.owner = owner;
    }

    public Integer getForksCount() {
        return forksCount;
    }

    public void setForksCount(Integer forksCount) {
        this.forksCount = forksCount;
    }

    public Integer getSubscribersCount() {
        return subscribersCount;
    }

    public void setSubscribersCount(Integer subscribersCount) {
        this.subscribersCount = subscribersCount;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSubscribersUrl() {
        return subscribersUrl;
    }

    public void setSubscribersUrl(String subscribersUrl) {
        this.subscribersUrl = subscribersUrl;
    }
}