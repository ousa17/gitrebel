package com.asana.rebel.data.source.remote.service;

import com.asana.rebel.BuildConfig;
import com.asana.rebel.data.models.GitRepo;
import com.asana.rebel.data.models.Owner;

import java.util.List;
import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Url;

public interface ServiceEndpoint {


    /*Url*/
    String BASE_URL = BuildConfig.BASE_URL;
    String LIST_GIT_REPO_URL = "repositories";

    @GET(LIST_GIT_REPO_URL)
    Observable<List<GitRepo>> getListRepo();

    @GET
    Observable<List<Owner>> getListSubscribers(@Url String subscribersUrl);
}
