package com.asana.rebel.data.source.local.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.NonNull;

import com.asana.rebel.data.models.GitRepo;
import com.asana.rebel.utils.ApplicationUtils;
import com.asana.rebel.utils.Logger;
import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.sql.SQLException;

public class DataBaseHelper extends OrmLiteSqliteOpenHelper {

    private static final String NAME = "BeeTheMove.db";
    //TODO LIVRAISON DB Version
    private static final int DB_VERSION = 3;

    public DataBaseHelper(@NonNull Context context) {
        super(context, ApplicationUtils.NAME + NAME, null, DB_VERSION);
    }

    public <T, ID> Dao<T, ID> getsDao(Class<T> clazz) throws SQLException {
        return getDao(clazz);
    }

    @Override
    public void onCreate(@NonNull SQLiteDatabase database, @NonNull ConnectionSource connectionSource) {
        try {
            TableUtils.createTable(connectionSource, GitRepo.class);
        } catch (Exception e) {
            Logger.error(this, e);
        }
    }

    @Override
    public void onUpgrade(@NonNull SQLiteDatabase database, @NonNull ConnectionSource connectionSource, int oldVersion, int newVersion) {
        try {
            TableUtils.dropTable(connectionSource, GitRepo.class, true);
            onCreate(database, connectionSource);
        } catch (Exception e) {
            Logger.error(this, e);
        }
    }

    @Override
    public void close() {
        super.close();
    }
}
