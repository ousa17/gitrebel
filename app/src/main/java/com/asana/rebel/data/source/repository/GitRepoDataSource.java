package com.asana.rebel.data.source.repository;

import com.asana.rebel.data.models.GitRepo;
import com.asana.rebel.data.models.Owner;

import java.util.List;

import io.reactivex.Observable;

/**
 * Created by user on 27/01/2018.
 */

public interface GitRepoDataSource {

    Observable<List<GitRepo>> getListRepo();

    Observable<List<Owner>> getListSubscribers(String subscribersUrl);

    }
