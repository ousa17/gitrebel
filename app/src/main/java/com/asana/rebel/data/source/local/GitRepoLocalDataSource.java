package com.asana.rebel.data.source.local;

import com.asana.rebel.data.models.GitRepo;
import com.asana.rebel.data.models.Owner;
import com.asana.rebel.data.source.local.database.BaseDAO;
import com.asana.rebel.data.source.local.database.DataBaseHelper;
import com.asana.rebel.data.source.repository.GitRepoDataSource;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Observable;

/**
 * Created by oussama on 27/01/2018.
 */

@Singleton
public class GitRepoLocalDataSource extends BaseDAO<GitRepo, String> implements GitRepoDataSource {

    @Inject
    public GitRepoLocalDataSource(DataBaseHelper dataBaseHelper) {
        super(dataBaseHelper, GitRepo.class);
    }


    @Override
    public Observable<List<GitRepo>> getListRepo() {
        return null;
    }

    @Override
    public Observable<List<Owner>> getListSubscribers(String subscribersUrl) {
        return null;
    }


}
